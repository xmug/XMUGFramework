using System;
using UnityEngine;

namespace XMUGFramework
{
    /// <summary>
    /// 定义参数的事件体，名字作为事件类型，内容作为参数
    /// </summary>
    public interface IEvent
    {
        DateTime sendTime { get; set; }
        Type senderType { get; set; }
        Type receiverType { get; set; }
    }

    public class XEvent : IEvent
    {
        public DateTime sendTime { get; set; }
        public Type senderType { get; set; }
        public Type receiverType { get; set; }

        protected XEvent(Type s = null, Type r = null)
        {
            sendTime = DateTime.Now;
            senderType = s;
            receiverType = r;
        }
    }
}
