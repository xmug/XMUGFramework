﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace XMUGFramework
{
    /// <summary>
    /// 事件注册对象
    /// </summary>
    public interface IRegistrations
    {
    }

    /// <summary>
    /// 接受以事件为参数的Action注册
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Registration<T> : IRegistrations
    {
        public Action<T> Action = obj => { };
    }

    public abstract class IEventBus : BaseCoreComponent
    {
        public abstract void Register<T>(Action<T> action) where T : IEvent;
        public abstract void UnRegister<T>(Action<T> action) where T : IEvent;
        public abstract void Trigger<T>(T eventObj) where T : IEvent;
    }

    public class EventBus : IEventBus
    {
        private readonly Dictionary<Type, IRegistrations> _registrations = new Dictionary<Type, IRegistrations>();

        public override void Init()
        {
        }

        public override void Register<T>(Action<T> action)
        {
            DictionaryQuery<T>(type =>
            {
                var reg = _registrations[type] as Registration<T>;
                reg.Action += action;
            }, type =>
            {
                var reg = new Registration<T>();
                reg.Action += action;
                _registrations.Add(type, reg);
            });
        }

        public override void UnRegister<T>(Action<T> action)
        {
            DictionaryQuery<T>(type =>
            {
                var reg = _registrations[type] as Registration<T>;
                reg.Action -= action;
                if (reg.Action.Equals(default)) _registrations.Remove(type);
            }, type => { Debug.LogWarning(type + " is not registered in the Event System"); });
        }

        public override void Trigger<T>(T eventObj)
        {
            DictionaryQuery<T>(type =>
            {
                var reg = _registrations[type] as Registration<T>;
                reg.Action?.Invoke(eventObj);
            }, type => { Debug.LogWarning(type + " is not registered in the Event System"); });
        }

        /// <summary>
        /// 这里的type 特指typeof的返回对象
        /// </summary>
        /// <param name="containsKeyAction"></param>
        /// <param name="unContainsKeyAction"></param>
        /// <typeparam name="T"></typeparam>
        private void DictionaryQuery<T>(Action<Type> containsKeyAction, Action<Type> unContainsKeyAction)
        {
            var type = typeof(T);
            if (_registrations.ContainsKey(type))
            {
                containsKeyAction.Invoke(type);
            }
            else
            {
                unContainsKeyAction.Invoke(type);
            }
        }
    }
}