﻿namespace XMUGFramework
{
    public interface IBaseCoreComponent
    {
        bool IsInit { get; set; }
        void Init();
    }

    public abstract class BaseCoreComponent : IBaseCoreComponent
    {
        /// <summary>
        /// 接口阉割
        /// </summary>
        bool IBaseCoreComponent.IsInit { get; set; }
        
        /// <summary>
        /// 暂时没想好是否要强制子类实现
        /// </summary>
        public abstract void Init();
    }
}