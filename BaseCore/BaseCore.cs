﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace XMUGFramework
{
    public interface IBaseCore
    {
        
    }
    
    public abstract class BaseCore<T> : IBaseCore where T : BaseCore<T>, new()
    {
        private static T _instance;

        private static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new T();
                    _instance.Init();
                }

                return _instance;
            }
        }

        // BaseCore 初始化
        protected BaseCore()
        {
            _iocContainer = new IOCContainer();
            AutoCleanUpRegister();
        }

        private IOCContainer _iocContainer;

        // IOC容器注册
        protected abstract void Init();

        public static T1 Get<T1>() where T1 : class, IBaseCoreComponent
        {
            var rt = Instance._iocContainer.Get<T1>();

            if (rt == null)
            {
                // 不是很好，等再改，hard-code了
                Debug.LogError(typeof(T1).IsSubclassOf(typeof(AManager))
                    ? typeof(T1) + " is not in the inspector"
                    : typeof(T1) + " is not Registered in Core");
            }
            else if (!rt.IsInit)
            {
                // 懒初始化
                rt.Init();
                rt.IsInit = true;
            }

            return rt;
        }
        
        // /// <summary>
        // /// GetAll Method will return all BaseCoreComponent under the T1 class, 
        // /// which means all child class of T1 and T1 itself will be returned, 
        // /// This method is not recommended to be used, but only if you know what you are going to do
        // /// </summary>
        // /// <typeparam name="T1"></typeparam>
        // /// <returns></returns>
        public static List<T1> GetAll<T1>() where T1 : class, IBaseCoreComponent
        {
            var rts = Instance._iocContainer.GetAll<T1>();
        
            if (rts == null)
            {
                Debug.LogError(typeof(T1).IsSubclassOf(typeof(AManager))
                               || typeof(T1) == typeof(AManager)
                    ? typeof(T1) + " are not in the inspector"
                    : typeof(T1) + " are not Registered in Core");
            }
            else
            {
                foreach (var rt in rts)
                {
                    if(!rt.IsInit)
                    {
                        rt.Init();
                        rt.IsInit = true;
                    }
                }
            }
            return rts;
        }

        /// <summary>
        /// Use Previous T1 as the type, or the deep type of instance
        /// By choosing useSpecificType or not
        /// By default, it's using deep type of instance
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="useSpecificType"></param>
        /// <typeparam name="T1"></typeparam>
        protected void Register<T1>(T1 instance, bool useSpecificType = false)
        {
            Instance._iocContainer.Register<T1>(instance, useSpecificType);
        }

        private void AutoCleanUpRegister()
        {
            // Automatic Scene Clean up when scene is changed
            SceneManager.sceneUnloaded += (pre) => { _instance = null; };
        }

        protected void AutoRegisterAllManagersInInspector()
        {
            AManager[] managers = Object.FindObjectsOfType<AManager>();
            foreach (var manager in managers)
            {
                Register(manager);
            }
        }
    }
}