﻿using System;
using System.Collections.Generic;
using UnityEngine;
using XMUGFramework.Command;

namespace XMUGFramework.Command
{
    // TODO: 出大问题，这东西是状态机，不是命令系统
    
    public abstract class ICommandSystem : BaseCoreComponent
    {
        public abstract TCommand Get<TCommand>() where TCommand : class, ICommand, new();
    }

    public class CommandSystem : ICommandSystem
    {
        private readonly Dictionary<Type, ICommand> _commands = new Dictionary<Type, ICommand>();

        public override void Init()
        {
        }

        public override TCommand Get<TCommand>()
        {
            TCommand command = null;
            
            DictionaryQuery<TCommand>(type =>
            {
                command = _commands[type] as TCommand;
            }, type =>
            {
                command = new TCommand();
                _commands.Add(type, command as ICommand);
            });

            return command;
        }

        /// <summary>
        /// 这里的type 特指typeof的返回对象
        /// </summary>
        /// <param name="containsKeyAction"></param>
        /// <param name="unContainsKeyAction"></param>
        /// <typeparam name="T"></typeparam>
        private void DictionaryQuery<T>(Action<Type> containsKeyAction, Action<Type> unContainsKeyAction)
        {
            var type = typeof(T);
            if (_commands.ContainsKey(type))
            {
                containsKeyAction.Invoke(type);
            }
            else
            {
                unContainsKeyAction.Invoke(type);
            }
        }
    }
}