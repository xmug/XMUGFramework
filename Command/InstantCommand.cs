﻿namespace XMUGFramework.Command
{
    /// <summary>
    /// InstantActionHandler is used when you need an action handler and it doesn't make sense that
    /// it would be in an "on" state. This is useful for animations which return the character to
    /// the same state after the animation plays.
    ///
    /// How this works under the hood: this class calls StartAction and then immediately calls
    /// EndAction. Some methods here are short-circuited: IsActive is always false, and you can
    /// always end the action, which does nothing.
    ///
    /// In order to implement an instant action handler you need to:
    ///
    /// 1. Pick a context type (or use EmptyContext) and inherit from this class e.g.:
    ///        public class MyInstantActionHandler : InstantActionHandler<float>
    ///
    /// 2. Implement CanStartAction, required by IActionHandler.
    ///
    /// 3. Implement _StartAction, required by BaseActionHandler.
    /// </summary>
    /// <typeparam name="TContext">Context type.</typeparam>
    public abstract class InstantCommand<TContext> : ACommand<TContext> where TContext : IContext
    {
        public override void StartAction(object context)
        {
            base.StartAction(context);
            base.EndAction(context);
        }

        protected override bool _IsActive(TContext context)
        {
            return false;
        }

        protected override bool _CanEndAction(TContext context)
        {
            return true;
        }

        protected override void _EndAction(TContext context) { }
    }
}