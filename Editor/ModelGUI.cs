﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace XMUGFramework
{
    public struct ExposedData
    {
        public object instance;
        public MemberInfo memberInfo;
        public object[] param;
        public string parentName;

        public ExposedData(object obj, MemberInfo info, object[] p = null, string pName = "")
        {
            instance = obj;
            memberInfo = info;
            param = p;
            parentName = pName;
        }
    }

    public class ModelGUI : EditorWindow
    {
        private List<ExposedData> exposedMembers = new List<ExposedData>();
        private Vector2 scrollPos;

        [MenuItem("XMUGFramework/ModelGUI")]
        private static void ShowWindow()
        {
            Rect wr = new Rect(0, 0, 500, 500);
            ModelGUI window =
                (ModelGUI) EditorWindow.GetWindowWithRect(typeof(ModelGUI), wr, true, "XMUGFramework Model Debugger");
            window.Show();
        }

        private void OnEnable()
        {
            var models = GameCore.GetAll<AModel>();

            foreach (var model in models)
            {
                var type = model.GetType();
                BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
                MemberInfo[] members = type.GetMembers(flags);

                foreach (var member in members)
                {
                    // As long as more than 0
                    var length = member.GetCustomAttributes(typeof(AExposedAttribute), false).Length;
                    if (length > 0)
                    {
                        if (length < 2)
                        {
                            if (member.GetCustomAttribute<ExposedClassAttribute>(false) != null)
                            {
                                switch (member.MemberType)
                                {
                                    case MemberTypes.Field:
                                        ClassInvoke(member as FieldInfo, model, model.GetType().Name);
                                        break;
                                    case MemberTypes.Property:
                                        ClassInvoke(member as PropertyInfo, model, model.GetType().Name);
                                        break;
                                }

                                continue;
                            }

                            exposedMembers.Add(new ExposedData(model, member, pName: model.GetType().Name));
                        }
                        else
                        {
                            Debug.LogError
                                ("There are more than 1 exposedAttribute on " + member.Name);
                        }
                    }
                }
            }
        }

        public void ClassInvoke(FieldInfo info, object instance, string parentName = "")
        {
            var valueObj = info.GetValue(instance);
            var type = valueObj.GetType();
            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            MemberInfo[] members = type.GetMembers(flags);

            foreach (var member in members)
            {
                // As long as more than 0
                var length = member.GetCustomAttributes(typeof(AExposedAttribute), false).Length;
                if (length > 0)
                {
                    if (length < 2)
                    {
                        if (member.GetCustomAttribute<ExposedClassAttribute>(false) != null)
                        {
                            switch (member.MemberType)
                            {
                                case MemberTypes.Field:
                                    ClassInvoke(member as FieldInfo, valueObj, parentName + " - " + info.Name);
                                    break;
                                case MemberTypes.Property:
                                    ClassInvoke(member as PropertyInfo, valueObj, parentName + " - " + info.Name);
                                    break;
                            }

                            continue;
                        }

                        exposedMembers.Add(new ExposedData(valueObj, member, pName: parentName + " - " + info.Name));
                    }
                    else
                    {
                        Debug.LogError
                            ("There are more than 1 exposedAttribute on " + member.Name);
                    }
                }
            }
        }

        public void ClassInvoke(PropertyInfo info, object instance, string parentName = "")
        {
            var valueObj = info.GetValue(instance);
            var type = valueObj.GetType();
            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            MemberInfo[] members = type.GetMembers(flags);

            foreach (var member in members)
            {
                // As long as more than 0
                var length = member.GetCustomAttributes(typeof(AExposedAttribute), false).Length;
                if (length > 0)
                {
                    if (length < 2)
                    {
                        if (member.GetCustomAttribute<ExposedClassAttribute>(false) != null)
                        {
                            switch (member.MemberType)
                            {
                                case MemberTypes.Field:
                                    ClassInvoke(member as FieldInfo, valueObj, parentName + " - " + info.Name);
                                    break;
                                case MemberTypes.Property:
                                    ClassInvoke(member as PropertyInfo, valueObj, parentName + " - " + info.Name);
                                    break;
                            }

                            continue;
                        }

                        exposedMembers.Add(new ExposedData(valueObj, member, pName: parentName + " - " + info.Name));
                    }
                    else
                    {
                        Debug.LogError
                            ("There are more than 1 exposedAttribute on " + member.Name);
                    }
                }
            }
        }

        public void OnGUI()
        {
            EditorGUILayout.LabelField("Exposed Fields", EditorStyles.boldLabel);
            EditorGUILayout.BeginVertical();
            scrollPos =
                EditorGUILayout.BeginScrollView(scrollPos);
            foreach (var variabMember in exposedMembers)
            {
                GetValue(variabMember);
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
        }

        private void GetValue(ExposedData data)
        {
            var attribute = data.memberInfo.GetCustomAttribute<AExposedAttribute>(false);
            switch (attribute)
            {
                case ExposedFieldAttribute i:
                    FieldInfo(data.memberInfo as FieldInfo, data.instance, data.parentName);
                    break;
                case ExposedPropertyAttribute i:
                    PropertyInfo(data.memberInfo as PropertyInfo, data.instance, data.param, data.parentName);
                    break;
                default:
                    Debug.LogWarning(attribute.GetType() + "This type of data is still not supported yet");
                    break;
            }
        }

        private void FieldInfo(FieldInfo info, object instance, string parentName = "")
        {
            var valueObj = info.GetValue(instance);

            switch (valueObj)
            {
                case int i:
                    info.SetValue(instance,
                        EditorGUILayout.IntField($"{parentName} - {info.Name}", i));
                    break;
                case float i:
                    info.SetValue(instance,
                        EditorGUILayout.FloatField($"{parentName} - {info.Name}", i));
                    break;
                case string i:
                    info.SetValue(instance,
                        EditorGUILayout.TextField($"{parentName} - {info.Name}", i));
                    break;
                case Vector3 i:
                    info.SetValue(instance,
                        EditorGUILayout.Vector3Field($"{parentName} - {info.Name}", i));
                    break;
                case Vector2 i:
                    info.SetValue(instance,
                        EditorGUILayout.Vector2Field($"{parentName} - {info.Name}", i));
                    break;
                case Enum i:
                    info.SetValue(instance,
                        EditorGUILayout.EnumFlagsField($"{parentName} - {info.Name}", i));
                    break;
                case IBindableProperty i:
                    var tmp = valueObj.GetType().GetProperty("Value", BindingFlags.Public | BindingFlags.Instance);
                    GetValue(new ExposedData(valueObj, tmp, pName: parentName + " - " + info.Name));
                    break;
                case IList i:
                    int length = i.Count;
                    for (int j = 0; j < length; j++)
                    {
                        PropertyInfo(valueObj.GetType().GetProperty("Item"), valueObj, new object[] {j},
                            parentName + " - " + info.Name + " [" + j + "]");
                        // GetValue(new ExposedData(valueObj, valueObj.GetType().GetProperty("Item"), new object[] {j}),
                        //     parentName + info.Name + " [" + j + "]");
                    }

                    break;
                default:
                    EditorGUILayout.TextField($"{parentName} - {info.Name}", valueObj.ToString());
                    break;
            }
        }

        private void PropertyInfo(PropertyInfo info, object instance, object[] param = null, string parentName = "")
        {
            var method = info.GetMethod;
            var valueObj = method.Invoke(instance, param);
            switch (valueObj)
            {
                case int i:
                    info.SetValue(instance,
                        EditorGUILayout.IntField($"{parentName} - {info.Name}", i), param);
                    break;
                case float i:
                    info.SetValue(instance,
                        EditorGUILayout.FloatField($"{parentName} - {info.Name}", i), param);
                    break;
                case string i:
                    info.SetValue(instance,
                        EditorGUILayout.TextField($"{parentName} - {info.Name}", i), param);
                    break;
                case Vector3 i:
                    info.SetValue(instance,
                        EditorGUILayout.Vector3Field($"{parentName} - {info.Name}", i), param);
                    break;
                case Vector2 i:
                    info.SetValue(instance,
                        EditorGUILayout.Vector2Field($"{parentName} - {info.Name}", i), param);
                    break;
                case Enum i:
                    info.SetValue(instance,
                        EditorGUILayout.EnumFlagsField($"{parentName} - {info.Name}", i), param);
                    break;
                case IBindableProperty i:
                    var tmp = valueObj.GetType().GetProperty("Value", BindingFlags.Public | BindingFlags.Instance);
                    GetValue(new ExposedData(valueObj, tmp, pName: parentName + " - " + info.Name));
                    break;
                case IList i:
                    int length = i.Count;
                    for (int j = 0; j < length; j++)
                    {
                        PropertyInfo(valueObj.GetType().GetProperty("Item"), valueObj, new object[] {j},
                            parentName + " - " + info.Name + " [" + j + "]");
                    }

                    break;
                default:
                    EditorGUILayout.TextField($"{parentName} - {info.Name}", valueObj.ToString());
                    break;
            }
        }


        // private SerializedObject serObj;
        // private List<SerializedProperty> serPtys;
        //
        // private void OnGUI()
        // {
        //     Debug.Log(serObj is null);
        //     if (serObj != null)
        //     {
        //         serObj.Update();
        //         EditorGUI.BeginChangeCheck();
        //         foreach (var serPty in serPtys)
        //         {
        //             EditorGUILayout.PropertyField(serPty, true);
        //         }
        //
        //         var ie = serObj.GetIterator();
        //         do
        //         {
        //             Debug.LogWarning(ie.type);
        //         } while (ie.Next(true));
        //
        //         if (EditorGUI.EndChangeCheck())
        //         {
        //             serObj.ApplyModifiedProperties();
        //         }
        //     }
        // }

        // public void Register(UnityEngine.Object obj)
        // {
        //     serObj = new SerializedObject(obj);
        //     serPtys = new List<SerializedProperty>();
        // }

        // // private void Update()
        // // {
        // //     Debug.Log("Update");
        // // }
        //
        // private void OnEnable()
        // {
        //     Debug.Log("OnEnable");
        // }
        //
        // private void OnDisable()
        // {
        //     Debug.Log("OnDisable");
        // }
        //
        // private void OnFocus()
        // {
        //     Debug.Log("当窗口获得焦点时调用一次");
        // }
        //
        // private void OnLostFocus()
        // {
        //     Debug.Log("当窗口丢失焦点时调用一次");
        // }
        //
        // private void OnHierarchyChange()
        // {
        //     Debug.Log("当Hierarchy视图中的任何对象发生改变时调用一次");
        // }
        //
        // private void OnProjectChange()
        // {
        //     Debug.Log("当Project视图中的资源发生改变时调用一次");
        // }
        //
        // private void OnInspectorUpdate()
        // {
        //     Debug.Log("窗口面板的更新");
        //     //这里开启窗口的重绘，不然窗口信息不会刷新
        //     this.Repaint();
        // }
        //
        // private void OnSelectionChange()
        // {
        //     //当窗口处于开启状态，并且在Hierarchy视图中选择某游戏对象时调用
        //     foreach (Transform t in Selection.transforms)
        //     {
        //         //有可能是多选，这里开启一个循环打印选中游戏对象的名称
        //         Debug.Log("OnSelectionChange" + t.name);
        //     }
        // }
        //
        // private void OnDestroy()
        // {
        //     Debug.Log("当窗口关闭时调用");
        // }
    }
}