﻿using System;

namespace XMUGFramework
{
    public abstract class AModel : BaseCoreComponent
    {
        public T AutoSaveLoad<T>(string name, T bindableProperty) where T : BindableProperty<T>, new()
        {
            var storage = PlayerPrefsStorage.Instance;
            bindableProperty.OnValueChanged += data =>
            {
                storage.SetValue(name, data);
            };

            return storage.GetValue(name, bindableProperty.Value);
        }
    }
}