﻿using System;

namespace XMUGFramework
{
    public abstract class AExposedAttribute : Attribute
    {
        
    }
    
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Enum)]
    public class ExposedFieldAttribute : AExposedAttribute
    {
        public ExposedFieldAttribute()
        {
        }
    }
    
    
    [AttributeUsage(AttributeTargets.Property)]
    public class ExposedPropertyAttribute : AExposedAttribute
    {
        public ExposedPropertyAttribute()
        {
        }
    }
    
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field)]
    public class ExposedClassAttribute : AExposedAttribute
    {
        public ExposedClassAttribute()
        {
        }
    }
    
    [AttributeUsage(AttributeTargets.All)]
    public class ExposedAllAttribute : AExposedAttribute
    {
        public ExposedAllAttribute()
        {
        }
    }
}