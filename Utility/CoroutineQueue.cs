﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XMUGFramework
{
    /// <summary>
    /// Coroutine Queue:
    /// A very useful and convenient tool to run different coroutine without management
    /// </summary>
    public class CoroutineQueue
    {
        private readonly uint maxActive;
        private readonly Func<IEnumerator, Coroutine> coroutineStarter;
        private readonly Queue<IEnumerator> queue;
        private uint numActive;

        public CoroutineQueue(uint maxActive, Func<IEnumerator, Coroutine> coroutineStarter)
        {
            if (maxActive == 0)
            {
                throw new ArgumentException("Must be at least one", "maxActive");
            }

            this.maxActive = maxActive;
            this.coroutineStarter = coroutineStarter;
            queue = new Queue<IEnumerator>();
        }

        public void Run(IEnumerator coroutine)
        {
            if (numActive < maxActive)
            {
                var runner = CoroutineRunner(coroutine);
                coroutineStarter(runner);
            }
            else
            {
                queue.Enqueue(coroutine);
            }
        }

        public void RunWithCallback(IEnumerator coroutine, System.Action callback)
        {
            // Duplicate to make sure the call back is right after certain coroutine
            Run(coroutine);
            Run(CoroutineCallback(callback));
        }

        private IEnumerator CoroutineCallback(System.Action callback)
        {
            callback();
            yield return null;
        }

        /// <summary>
        /// This is a Enumerator managing the coroutine queue executing
        /// </summary>
        /// <param name="coroutine"></param>
        /// <returns></returns>
        private IEnumerator CoroutineRunner(IEnumerator coroutine)
        {
            numActive++;
            // Manually execute the child coroutine step by step until there is no yield key word inside of it
            while (coroutine.MoveNext())
            {
                yield return coroutine.Current;
            }
            numActive--;
            if (queue.Count > 0)
            {
                var next = queue.Dequeue();
                Run(next);
            }
        }
    }
}