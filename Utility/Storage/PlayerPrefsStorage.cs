﻿using UnityEngine;
using static UnityEngine.PlayerPrefs;

namespace XMUGFramework
{
    public class PlayerPrefsStorage : AStorage
    {
        protected override void Save()
        {
            PlayerPrefs.Save();
        }
        
        protected override bool HasKey(string key)
        {
            return PlayerPrefs.HasKey(key);
        }

        
        protected override void SetInt(string key, int value)
        {
            PlayerPrefs.SetInt(key,value);
        }

        protected override void SetFloat(string key, float value)
        {
            PlayerPrefs.SetFloat(key,value);
        }

        protected override void SetString(string key, string value)
        {
            PlayerPrefs.SetString(key,value);
        }

        protected override int GetInt(string key, int defaultValue)
        {
            return PlayerPrefs.GetInt(key, defaultValue);
        }

        protected override float GetFloat(string key, float defaultValue)
        {
            return PlayerPrefs.GetFloat(key, defaultValue);
        }

        protected override string GetString(string key, string defaultValue)
        {
            return PlayerPrefs.GetString(key, defaultValue);
        }
    }
}