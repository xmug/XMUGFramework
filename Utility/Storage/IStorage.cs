using UnityEngine;

namespace XMUGFramework
{
    public interface IStorage
    {
        /// <summary>
        /// Set
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetValue<T>(string key, T value);


        /// <summary>
        /// Get
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public T GetValue<T>(string key, T value);
    }

    public abstract class AStorage : Singleton<AStorage>, IStorage
    {
        protected abstract void Save();
        protected abstract bool HasKey(string key);
        protected abstract void SetInt(string key, int value);
        protected abstract void SetFloat(string key, float value);
        protected abstract void SetString(string key, string value);
        protected abstract int GetInt(string key, int defaultValue);
        protected abstract float GetFloat(string key, float defaultValue);
        protected abstract string GetString(string key, string defaultValue);

        public void SetValue<T>(string key, T value)
        {
            switch (value)
            {
                case int i:
                    SetInt(key, i);
                    break;
                case float i:
                    SetFloat(key, i);
                    break;
                case string i:
                    SetString(key, i);
                    break;
                case Vector3 i:
                    SetFloat(key + "_x", i.x);
                    SetFloat(key + "_y", i.y);
                    SetFloat(key + "_z", i.z);
                    break;
                case Vector2 i:
                    SetFloat(key + "_x", i.x);
                    SetFloat(key + "_y", i.y);
                    break;
                case bool i:
                    SetInt(key, i ? 1 : 0);
                    break;
                default:
                    Debug.LogError("This format of data is not supported by Storage System yet: " + value.GetType());
                    break;
            }
        }

        public T GetValue<T>(string key, T value)
        {
            object res = null;
            switch (value)
            {
                case int i:
                    res = GetInt(key, i);
                    break;
                case float i:
                    res = GetFloat(key, i);
                    break;
                case string i:
                    res = GetString(key, i);
                    break;
                case Vector3 i:
                    res = new Vector3(GetFloat(key + "_x", i.x),
                                    GetFloat(key + "_y", i.y),
                                    GetFloat(key + "_z", i.z));
                    break;
                case Vector2 i:
                    res = new Vector2(GetFloat(key + "_x", i.x),
                                    GetFloat(key + "_y", i.y));
                    break;
                case bool i:
                    res = GetInt(key, i ? 1 : 0) > 0;
                    break;
                default:
                    Debug.LogError("This format of data is not supported by Storage System yet: " + value.GetType());
                    break;
            }

            return res is T ? (T) res : default;
        }
    }
}