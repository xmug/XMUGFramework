﻿using System.Collections.Generic;
using UnityEngine;

namespace XMUGFramework.ObjectPool
{
    public class ObjectPool
    {
        public ObjectPool()
        {
            pool = new Dictionary<string, List<GameObject>>();
        }

        //对象池
        private Dictionary<string, List<GameObject>> pool;

        // //通过对象池生成游戏对象
        // public GameObject SpawnObject(GameObject gameObject)
        // {
        //     // GameObject needObj;
        //     // //查看是否有该名字所对应的子池，且子池中有对象
        //     // if (pool.TryGetValue(gameObject.name, out var values))
        //     // {
        //     //     if (values.Count > 0)
        //     //     {
        //     //         //将0号对象返回
        //     //         needObj = values[0];
        //     //         //将0号对象从List中移除
        //     //         values.RemoveAt(0);
        //     //     }
        //     // }
        //     // else
        //     // {
        //     //     //直接通过Instantiate生成
        //     //     needObj = 
        //     //     //修改名称(去掉Clone)
        //     //     needObj.name = name;
        //     // }
        //
        //     // //设置为激活
        //     // needObj.SetActive(true);
        //     // //返回
        //     // return needObj;
        // }

        /// 
        /// 回收游戏对象到对象池
        /// 
        public void RecycleObj(GameObject Objname)
        {
            //防止被看到，设置为非激活哦
            Objname.SetActive(false);
            if (pool.ContainsKey(Objname.name))
            {
                //将当前对象放入对象子池
                pool[Objname.name].Add(Objname);
            }
            else
            {
                //创建该子池并将对象放入
                pool.Add(Objname.name, new List<GameObject> {Objname});
            }
        }
    }
}