﻿using System.Collections.Generic;
using UnityEngine;

namespace XMUGFramework.ObjectPool
{
    public class AssetsPool
    {
        // TODO: 实现objectPool
        
        protected AssetsPool()
        {
            assetsCache = new Dictionary<string, Object>();
        }
        //缓存字典
        private Dictionary<string, Object> assetsCache;

        //获取资源
        public virtual T GetAssets<T>(string path) where T : Object
        {
            //先查看缓存池中有没有这个资源
            if (assetsCache.ContainsKey(path))
            {
                //直接将这个资源返回
                return assetsCache[path] as T;
            }
            else
            {
                //通过Resource.Load去加载资源
                T assets = Resources.Load<T>(path);
                //将新资源加载到缓存池里
                assetsCache.Add(path,assets);
                //返回资源
                return assets;
            }
        }
        //卸载未使用的资源
        public void UnloadUnusedAssets()
        {
            //卸载
            Resources.UnloadUnusedAssets();
        }
    }
}