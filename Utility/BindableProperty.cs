using System;
using System.Collections.Generic;

namespace XMUGFramework
{
    public interface IBindableProperty
    {
        
    }
    
    public class BindableProperty<T> : IBindableProperty where T : new()
    {
        private T _value;
        private bool notifyOnInit;

        public BindableProperty()
        {
        }

        [ExposedProperty]
        public T Value
        {
            get => _value;
            set
            {
                if (_value == null && !notifyOnInit)
                {
                    _value = value;
                }
                
                if (!EqualityComparer<T>.Default.Equals(value, _value))
                {
                    _value = value;
                    
                    OnValueChanged?.Invoke(_value);
                }
            }
        }

        public event Action<T> OnValueChanged;
    }
}